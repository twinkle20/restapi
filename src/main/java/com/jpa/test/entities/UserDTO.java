package com.jpa.test.entities;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


public class UserDTO 
{
	@NotEmpty(message = "firstName cannot be empty")
	@Size(min=4,max=10,message = "should contain characters between 4 to 10")
	private String firstName;
	
	@NotEmpty(message = "lastName cannot be empty") 
	@Size(min=4,max=10,message = "should contain characters between 4 to 10")
	private String lastName;
	
	private int id;
		
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}	
	
}
