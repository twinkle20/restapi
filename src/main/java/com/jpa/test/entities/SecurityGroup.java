package com.jpa.test.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="Security_Group")
public class SecurityGroup 
{
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	
	@Column(name="GroupName")
	@Size(min=4, max=10, message="should contain characters between 4 to 10")
	@NotEmpty(message = "GroupName cannot be empty")
	private String groupName;
	
	@Column(name="_Enable")
	@NotNull(message = "enable cannot be null")
	private boolean enable;
	
	@Column(name="_Delete")
	@NotNull(message = "delete cannot be null")
	private boolean delete;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public boolean isDelete() {
		return delete;
	}
	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	
	public SecurityGroup(int id, String groupName, boolean enable, boolean delete) {
		super();
		this.id = id;
		this.groupName = groupName;
		this.enable = enable;
		this.delete = delete;
	}
	
	public SecurityGroup() {
		super();
	}
	
	@Override
	public String toString() {
		return "SecurityGroup [id=" + id + ", groupName=" + groupName + ", enable=" + enable + ", delete=" + delete
				+ "]";
	}
	
}
