package com.jpa.test.entities;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

public class LoginDTO 
{
	@NotEmpty(message = "email cannot be empty")
	@Email(message = "Invalid emailId - follow proper syntax ") 
	private String email;
	
	@NotEmpty(message = "Password cannot be empty")
	@Size(min=8, max=16, message = "password should be atleast 8 character long")
	private String password;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
