package com.jpa.test.entities;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name="User")
public class User 
{
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	@Column(name="First_Name")
	@NotEmpty
	@Size(min=5,max=10)
	private String firstName;
	
	@Column(name="Last_Name")
	@NotEmpty
	@Size(min=5,max=10)
	private String lastName;
	
	@Column(name="Email")
	@Email
	private String email;
	
	@Column(name="Password")
	@Size(min=8,max=16)
	private String password;
	
	@Column(name="_enable")
	@NotNull
	private boolean enable;
	
	@Column(name="_delete")
	@NotNull
	private boolean delete;
	
	@Column(name="isLoggedIn")
	private Boolean isLoggedIn;
	
	@OneToMany(targetEntity = SecurityGroup.class, cascade=CascadeType.ALL)
	private List<SecurityGroup> security;
	
	@OneToOne(targetEntity = Address.class, cascade = CascadeType.ALL)
	@JoinColumn(name="address_id")
	private Address address;

	public List<SecurityGroup> getSecurity() {
		return security;
	}

	public void setSecurity(List<SecurityGroup> security) {
		this.security = security;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean getEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public boolean getDelete() {
		return delete;
	}

	public void setDelete(boolean delete) {
		this.delete = delete;
	}
	

	public Boolean isLoggedIn() {
		return isLoggedIn;
	}

	public void setLoggedIn(Boolean isLoggedIn) {
		this.isLoggedIn = isLoggedIn;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User() {
		super();
	}

	public User(int id, String firstName, String lastName, String email, String password, boolean enable,
			boolean delete, boolean isLoggedIn, List<SecurityGroup> security, Address address) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.password = password;
		this.enable = enable;
		this.delete = delete;
		this.isLoggedIn=isLoggedIn;
		this.security = security;
		this.address = address;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", password=" + password + ", enable=" + enable + ", delete=" + delete + ", security=" + security
				+ ", address=" + address + "]";
	}   

}
