package com.jpa.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.jpa.test.entities.User;
import com.jpa.test.entities.UserDTO;
import com.jpa.test.service.UserService;

import io.swagger.annotations.Api;

@RestController
@Api
public class UserController 
{ 
	@Autowired  
	UserService userService; 
	  
	@GetMapping("/user")  //READ
	private List<User> getAllUsers()   
	{  
		return userService.getAllUsers();  
	}  
	
	
	@GetMapping("/user/{id}")   //READ
	private ResponseEntity<Object> getUser(@PathVariable("id") int id)  
	{  
		return userService.getUserById(id);  
	} 
	
	
	@DeleteMapping("/user/{id}")   //DELETE
	private ResponseEntity<Object> deleteUser(@PathVariable int id)   
	{  
		 return userService.delete(id);
	} 
	
	@PostMapping("/user")
	private User createUser(@Valid @RequestBody UserDTO userDTO)
	{
		return userService.createUsers(userDTO);
	}
	  
	@PutMapping("/user")   //UPDATE
	private User update(@Valid @RequestBody User user)   
	{  
		 userService.update(user);
		 return user;
	}  

}
