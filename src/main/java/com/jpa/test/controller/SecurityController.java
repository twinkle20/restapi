package com.jpa.test.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jpa.test.entities.SecurityGroup;
import com.jpa.test.service.SecurityService;

@RequestMapping("/api")
@RestController
public class SecurityController 
{
	@Autowired
	SecurityService securityService;
	
	@GetMapping("/security")  //READ
	private List<SecurityGroup> getAllSecurities()   
	{  
		return securityService.getAllSecurity();  
	}  
	
	
	@GetMapping("/security/{id}")   //READ BY ID
	private ResponseEntity<Object> getSecurity(@PathVariable("id") int id)   
	{  
		return securityService.getSecurityById(id);  
	} 
	
	
	@DeleteMapping("/security/{id}")   //DELETE
	private ResponseEntity<Object> deleteSecurity(@PathVariable int id)   
	{  
		 return securityService.delete(id);
	} 
	
	
	@PostMapping("/security")    //CREATE
	private SecurityGroup createSecurity(@Valid @RequestBody SecurityGroup securityGroup)   
	{  
		this.securityService.createSecurity(securityGroup); 
		return securityGroup;	  
	}  
	
	  
	@PutMapping("/security")   //UPDATE
	private SecurityGroup updateSecurity(@Valid @RequestBody SecurityGroup securityGroup)   
	{  
		securityService.update(securityGroup);  
		return securityGroup;  
	}  
}
