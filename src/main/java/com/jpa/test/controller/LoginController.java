package com.jpa.test.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import com.jpa.test.entities.LoginDTO;
import com.jpa.test.service.LoginService;

import io.swagger.annotations.Api;

@RestController
@Api
public class LoginController 
{
	@Autowired
	LoginService loginService;
	
	@PostMapping("/login")
    public ResponseEntity<Object> LoginUser(@Valid @RequestBody LoginDTO loginDTO)throws Exception
    {
		return loginService.loginUser(loginDTO);
    }
	
	@PostMapping("/logout")
	public ResponseEntity<Object> LogoutUser(@Valid @RequestBody LoginDTO loginDTO)
	{
		return loginService.logoutUser(loginDTO);
	}
}
