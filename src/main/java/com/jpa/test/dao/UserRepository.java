package com.jpa.test.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpa.test.entities.User;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> 
{
	public User findByEmail(String email);
	boolean existsByEmail(String email); 
	boolean existsByPassword(String password);
}
