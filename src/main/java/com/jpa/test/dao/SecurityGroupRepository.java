package com.jpa.test.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.jpa.test.entities.SecurityGroup;

@Repository
public interface SecurityGroupRepository extends JpaRepository<SecurityGroup, Integer> {

}
