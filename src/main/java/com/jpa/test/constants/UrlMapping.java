package com.jpa.test.constants;

public class UrlMapping {
	
    public static final String SWAGGER_API_DOCS = "/v2/api-docs/**";
    public static final String SWAGGER_JSON = "/swagger.json";
    public static final String SWAGGER_UI_HTML = "/swagger-ui.html";
    public static final String SWAGGER_WEBJARS = "/webjars/**";
    public static final String SWAGGER_RESOURCES = "/swagger-resources/**";

	private UrlMapping() {}
}
