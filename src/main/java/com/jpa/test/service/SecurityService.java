package com.jpa.test.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.jpa.test.dao.SecurityGroupRepository;
import com.jpa.test.entities.SecurityGroup;

@Service
public class SecurityService 
{
	@Autowired
	SecurityGroupRepository securityRepository;
	
	//Create :
	public SecurityGroup createSecurity(SecurityGroup securityGroup)
	{	
		return securityRepository.save(securityGroup);		
	}
		
	//Read :
	public List<SecurityGroup> getAllSecurity()      
	{   
		return securityRepository.findAll();  
	}  
		
	//GetByID :
	public ResponseEntity<Object> getSecurityById(int id)
	{		
		Optional<SecurityGroup> optional = securityRepository.findById(id);
	    return ResponseEntity.ok(optional.get());		
	}  
		  
		
	//Delete :
	public ResponseEntity<Object> delete(int id)   
	{			
		if(securityRepository.findById(id).isPresent()) 
		{
			securityRepository.deleteById(id);
			if (securityRepository .findById(id).isPresent()) 
			{
				return ResponseEntity.unprocessableEntity().body("Failed to "
				+ "delete the specified record");
		    } 
			else 
				return ResponseEntity.ok().body("Successfully deleted");
		 } 
		else
			return ResponseEntity.unprocessableEntity().body("No Records Found");	
	} 
		
		
	//Update :
	public SecurityGroup update(SecurityGroup securityGroup)
	{  
		SecurityGroup existing=securityRepository.findById(securityGroup.getId()).get();
		existing.setGroupName(securityGroup.getGroupName());
		existing.setEnable(true);
		existing.setDelete(false);
		
		return securityRepository.save(existing);			
	}  
		
}
