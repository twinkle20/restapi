package com.jpa.test.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jpa.test.basic.auth.JwtResponse;
import com.jpa.test.basic.auth.JwtUtil;
import com.jpa.test.dao.UserRepository;
import com.jpa.test.entities.LoginDTO;
import com.jpa.test.entities.User;

@Service
public class LoginService 
{
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	private UserDetailsServiceImpl userDetailsServiceImpl;
	
	@Autowired
	private JwtUtil jwtUtil;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	User user;
	
	public ResponseEntity<Object> loginUser(LoginDTO loginDTO)throws Exception
	{
		System.out.println(loginDTO);		
		try 
		{
			this.authenticationManager.authenticate(new UsernamePasswordAuthenticationToken
					(loginDTO.getEmail(),loginDTO.getPassword()));
		} 
		catch (UsernameNotFoundException e) 
		{
			e.printStackTrace();
			throw new Exception("USER NOT FOUND");
		}
		catch(BadCredentialsException e)
		{
			e.printStackTrace();
			throw new Exception("BAD CREDENTIALS");
		}
		
		//fine......

		 UserDetails userDetails = userDetailsServiceImpl.loadUserByUsername(loginDTO.getEmail());
		 System.out.println(userDetails.getUsername());
		 String token = jwtUtil.generateToken(userDetails);
		 System.out.println("JWT "+token);
     	 return ResponseEntity.ok(new JwtResponse(token));

			
//		boolean email=userRepository.existsByEmail(loginDTO.getEmail());
//		//userRepository.findByEmail(loginDTO.getPassword());
//		
//		boolean pswd=userRepository.existsByPassword(loginDTO.getPassword());
//		if(email==false)
//			return ResponseEntity.unprocessableEntity().body("EmailId does not"
//						+  "exist");
//		if(pswd==false)
//			return ResponseEntity.unprocessableEntity().body("Password "
//						+ "mismatch");	
//		if(user.isLoggedIn()==false || user.isLoggedIn()==null)
//		{	
//		   user.setLoggedIn(true);
//		   userRepository.save(user);
//		}   
//		return ResponseEntity.ok().body("Successfully Login..!!");
	}
	
	public ResponseEntity<Object> logoutUser(LoginDTO loginDTO) 
	{
		userRepository.findByEmail(loginDTO.getEmail());
		if (user.isLoggedIn()==true || user.isLoggedIn()==null) 
		{
			user.setLoggedIn(false);
	        userRepository.save(user);
	        return ResponseEntity.ok().body("Successfully Logout..!!") ; 
	    }        
	    return ResponseEntity.unprocessableEntity().body("Error in Logging out");
	}

}
