package com.jpa.test.service;

import org.springframework.stereotype.Service;

import com.jpa.test.dao.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.ArrayList;


@Service
public class UserDetailsServiceImpl implements UserDetailsService
{
	@Autowired
	private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        com.jpa.test.entities.User user=userRepository.findByEmail(email);
        if (user== null) {
            throw new UsernameNotFoundException("User not found with this email Id");
        }
        return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
    }

}
