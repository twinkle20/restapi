package com.jpa.test.service;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jpa.test.dao.SecurityGroupRepository;
import com.jpa.test.dao.UserRepository;
import com.jpa.test.entities.SecurityGroup;
import com.jpa.test.entities.User;
import com.jpa.test.entities.UserDTO;

@Service
public class UserService 
{
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private SecurityGroupRepository securityRepository;
	
	@Autowired
	private JavaMailSender javaMailSender;
	
	//Create 
	public User createUsers(UserDTO userDTO)
	{		
		User user=new User();
		user.setFirstName(userDTO.getFirstName());
		user.setLastName(userDTO.getLastName());
		List<SecurityGroup> list=new ArrayList<>();
		Optional<SecurityGroup> securityId=securityRepository.findById(userDTO.getId());	
		SecurityGroup security=securityId.get();
		list.add(security);
		user.setSecurity(list);	
		
		SimpleMailMessage msg=new SimpleMailMessage();
		msg.setTo("twinkle.rawat@oodlestechnologies.com");
		msg.setSubject("Spring Boot Testing");
		msg.setText("User created Successfully..!!\n" + user.getFirstName() + 
		            user.getLastName() + user.getSecurity());
		javaMailSender.send(msg);
		
		return userRepository.save(user);
	}
	
	public UserDetails loadUserByUsername(String userEmail) throws UsernameNotFoundException {
        User userData = userRepository.findByEmail(userEmail);
        return new org.springframework.security.core.userdetails.User(userData.getEmail(), userData.getPassword(), new ArrayList());
    }
	
	//Read :
	public List<User> getAllUsers()      
	{   
		return userRepository.findAll();  
	}  
	
	//GetByID :
	public ResponseEntity<Object> getUserById(int id)
	{		
		Optional<User> optional = userRepository.findById(id);		
	    return ResponseEntity.ok(optional.get());		
	}  
	  	
   //Delete : 
	public ResponseEntity<Object> delete(int id)
	
	{  		
		 if(userRepository.findById(id).isPresent()) 
		 {
			 userRepository.deleteById(id);
			 if (userRepository .findById(id).isPresent()) 
			 {
				 return ResponseEntity.unprocessableEntity().body("Failed to "
	     		+ "delete the specified record");
			 } 
			 else return ResponseEntity.ok().body("Successfully deleted");
	     } 
		 else return ResponseEntity.unprocessableEntity().body("No Records Found");
	} 
		
	//Update :
	public User update(User user)
	{  
		User existing=userRepository.findById(user.getId()).get();			
		existing.setFirstName(user.getFirstName());
		existing.setLastName(user.getLastName());
		existing.setEmail(user.getEmail());
		existing.setPassword(user.getPassword());
		existing.setEnable(user.getEnable());
		existing.setDelete(user.getDelete());		
		existing.setSecurity(user.getSecurity());				
		existing.setAddress(user.getAddress());	    
		return userRepository.save(existing);					
	}
	
}
