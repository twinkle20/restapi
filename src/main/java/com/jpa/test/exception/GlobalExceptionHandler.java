package com.jpa.test.exception;

import java.util.NoSuchElementException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import java.time.LocalDateTime;
import org.springframework.web.bind.MethodArgumentNotValidException;


@ControllerAdvice
public class GlobalExceptionHandler 

{
	@ExceptionHandler(NoSuchElementException.class)
	public ResponseEntity<Object> handleExceptions(NoSuchElementException exception, WebRequest webRequest)
	{
		ExceptionResponse response = new ExceptionResponse();
		response.setDateTime(LocalDateTime.now());
		response.setMessage("No security with this Id is present");
		ResponseEntity<Object> entity = new ResponseEntity<>(response,HttpStatus.INTERNAL_SERVER_ERROR);
		return entity;
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<Object> handleExceptions(MethodArgumentNotValidException exception, WebRequest webRequest)
	{
		ExceptionResponse response = new ExceptionResponse();
		response.setDateTime(LocalDateTime.now());
		BindingResult bindingResult=exception.getBindingResult();		
		response.setMessage(bindingResult.getFieldError().getDefaultMessage());
		ResponseEntity<Object> entity = new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
		return entity;
    }
	
	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<Object> handleExceptions(HttpMessageNotReadableException exception, WebRequest webRequest)
	{
		ExceptionResponse response = new ExceptionResponse();
		response.setDateTime(LocalDateTime.now());
		response.setMessage("JSON Parse Error - Expecting only true or false");
		ResponseEntity<Object> entity = new ResponseEntity<>(response,HttpStatus.BAD_REQUEST);
		return entity;
    }
	
	
		
}
