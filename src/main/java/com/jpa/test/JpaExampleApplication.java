package com.jpa.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebSecurity
@EnableSwagger2
@SpringBootApplication
public class JpaExampleApplication {

	public static void main(String[] args) {
	SpringApplication.run(JpaExampleApplication.class, args);
	
	}
	
	
	
}
