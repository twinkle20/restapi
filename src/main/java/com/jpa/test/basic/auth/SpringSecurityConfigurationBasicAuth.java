package com.jpa.test.basic.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import static com.jpa.test.constants.UrlMapping.*;
import com.jpa.test.service.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfigurationBasicAuth extends WebSecurityConfigurerAdapter
{
	@Autowired
	private UserDetailsServiceImpl userDetailService;
	
	@Autowired
	private JwtAuthenticationFilter jwtFilter;
	
	@Override
	protected void configure(HttpSecurity http) throws Exception 
	{
		 http.cors().and().csrf().disable().authorizeRequests()
	     	.antMatchers("/login").permitAll()
	     	.antMatchers(SWAGGER_API_DOCS).permitAll()
	     	.antMatchers(SWAGGER_JSON).permitAll()
	     	.antMatchers(SWAGGER_RESOURCES).permitAll()
	     	.antMatchers(SWAGGER_UI_HTML).permitAll()
	     	.antMatchers(SWAGGER_WEBJARS).permitAll()
	     	.anyRequest().authenticated()
	     	.and()
	     	// this disables session creation on Spring Security
	     	.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
		 http.addFilterBefore(jwtFilter, UsernamePasswordAuthenticationFilter.class);
	 }
	 
	 @Override
	 public void configure(AuthenticationManagerBuilder auth) throws Exception {
	        auth.userDetailsService(userDetailService);   
	 }
	 
	 @Bean
	 public PasswordEncoder passwordEncoder()
	 {
		 return NoOpPasswordEncoder.getInstance();
	 }
	 
	 @Override
	 @Bean
	 public AuthenticationManager authenticationManagerBean() throws Exception
	 {
		 return super.authenticationManagerBean();
	 }
}
